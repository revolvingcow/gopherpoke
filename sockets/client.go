package sockets

import (
	"fmt"
	"io"

	"gitlab.com/revolvingcow/gopherpoke/models"
	"golang.org/x/net/websocket"
)

const channelBufSize = 512

var nextID int = 0

type Client struct {
	id     int
	conn   *websocket.Conn
	server *Server
	ch     chan *models.Payload
	done   chan bool
}

func NewClient(ws *websocket.Conn, server *Server) *Client {
	if ws == nil {
		panic("websocket not initialized")
	}

	if server == nil {
		panic("server not initialized")
	}

	nextID++

	return &Client{
		nextID,
		ws,
		server,
		make(chan *models.Payload, channelBufSize),
		make(chan bool),
	}
}

func (c *Client) Conn() *websocket.Conn {
	return c.conn
}

func (c *Client) Write(payload *models.Payload) {
	select {
	case c.ch <- payload:
	default:
		c.server.Del(c)
		err := fmt.Errorf("client %d is disconnected.", c.id)
		c.server.Err(err)
	}
}

func (c *Client) Done() {
	c.done <- true
}

// Listen Write and Read request via chanel
func (c *Client) Listen() {
	go c.listenWrite()
	c.listenRead()
}

// Listen write request via chanel
func (c *Client) listenWrite() {
	for {
		select {

		// send message to the client
		case payload := <-c.ch:
			_ = websocket.JSON.Send(c.conn, payload)

		// receive done request
		case <-c.done:
			c.server.Del(c)
			c.done <- true // for listenRead method
			return
		}
	}
}

// Listen read request via chanel
func (c *Client) listenRead() {
	for {
		select {

		// receive done request
		case <-c.done:
			c.server.Del(c)
			c.done <- true // for listenWrite method
			return

		// read data from websocket connection
		default:
			var payload models.Payload
			err := websocket.JSON.Receive(c.conn, &payload)
			if err == io.EOF {
				c.done <- true
			} else if err != nil {
				c.server.Err(err)
			} else {
				c.server.SendAll(&payload)
			}
		}
	}
}
