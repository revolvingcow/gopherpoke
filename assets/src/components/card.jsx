import { h, render, Component } from 'preact'
import socket from '../socket'
import payload, { actions } from '../payload'

export default class Card extends Component {
  onClick (event) {
    const value = event.target.value
    if (socket.init(document.location.host)) {
      socket.send(actions.PointStory, payload.point(0, value))
    }
  }

  render ({ value = '?' }) {
    const klass = 'card playing-card ' + background(value)
    return (
      <button className={klass} value={value} onClick={this.onClick}>
        <span className="playing-card-value top">{value}</span>
        <span className="playing-card-value bottom">{value}</span>
      </button>
    )
  }
}

const background = (value) => {
  switch (value.toLowerCase()) {
  case ':coffee:':
    return 'coffee'
  case '3':
    return 'witch'
  case '8':
    return 'guitar'
  case '21':
    return 'ref'
  case '0':
    return 'run'
  case '1':
    return 'swim'
  default:
    return 'mega'
  }
}
