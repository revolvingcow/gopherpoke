import { h, render, Component } from 'preact'
import Roster from './roster'
import Deck from './deck'
import Backlog from './backlog'

export default class App extends Component {
  render () {
    const players = [
      { nick: 'bmallred', email: 'bryan@revolvingcow.com' },
      { nick: 'floresj', email: 'john.s.flores@gmail.com' }
    ]

    const backlog = [
      {
        repo: 'https://github.com/bmallred/gopherpoke',
        issue: '1',
        title: 'Initial commit',
        description: 'I want it all now!',
        score: '8'
      },
      {
        repo: 'https://github.com/bmallred/gopherpoke',
        issue: '2',
        title: 'Second commit',
        description: 'Ugh...',
        score: ''
      }
    ]

    return (
      <div className="app">
        <Deck />
        <Backlog stories={backlog} />
        <Roster players={players} />
      </div>
    )
  }
}
