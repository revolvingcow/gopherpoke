import { h, render, Component } from 'preact'

export default class Story extends Component {
  render ({ repo = '', issue = '', title = '', description = '', score = '' }) {
    let link = null
    if (repo && issue) {
      const uri = repo + '/issues/' + issue
      link = <a href={uri} title="View the issue" target="_blank">View issue</a>
    }

    return (
      <span className="card story">
        <span className="story-score">{score}</span>
        <span className="story-title">{title}</span>
        <span className="story-link">{link}</span>
        <span className="story-description">{description}</span>
      </span>
    )
  }
}
