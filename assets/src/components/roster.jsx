import { h, render, Component } from 'preact'
import Player from './player'

export default class Roster extends Component {
  render ({ players = [] }) {
    const children = players.map(p => {
      return <Player email={p.email} nick={p.nick} />
    })

    return (<span className="roster">{children}</span>)
  }
}
