import { h, render, Component } from 'preact'
import Story from './story'

export default class Backlog extends Component {
  render ({ stories = [] }) {
    const children = stories.map(story => {
      return <Story repo={story.repo}
                    issue={story.issue}
                    title={story.title}
                    description={story.description}
                    score={story.score} />
    })

    return (<span className="backlog">{children}</span>)
  }
}
